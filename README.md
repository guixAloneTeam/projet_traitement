# README #

Ce projet de traitement d'image a été effectué dans le cadre de l'UE "introduction au traitement d'image numérique" 
lors de mon master 1 ISICG (informatique, synthèse d'images et conception graphique). 

Différentes fonctions ont été implémentées au fur et a mesure de l'année telle que les filtres de sobel, l'application 
de noyau de convolution, la détection de cercle ou d'ellipse avec l'algorithme de Hough ou bien des applications 
morphologiques telles que la squelettisation ou bien l'érosion & dilatation. 

Dans le but de développer mes capacités en FXML, dans le repository "GUI" une petite interface est disponible. 