package ProjetFlechette;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class MainApplication {

	public static void main(String[] args) {
		BufferedImage img = null;
		
		//lecture image
		try 
		{
			img = ImageIO.read(new File("image_test/img22.png"));
		} 
		catch (IOException e) 
		{
		    e.printStackTrace();
		}
		
		System.out.println("Notre image fait "+img.getHeight() +" x "+ img.getWidth());
		ImagePerso image = new ImagePerso(img);
		
		EllipseDetection ed = new EllipseDetection();
		ed.detectEllipse(image, image.width, image.height, 5, 1,0);
		System.out.println("Il y a " + ed.ellipses.size() +" ellipses !");
		
		BufferedImage tmp = image.convertFromImage();
		
		//enregistrement
		File outputfile = new File("result.png");
		try
		{
			ImageIO.write(tmp, "png", outputfile);
		}
		catch (Exception e)
		{
			System.out.println(e.toString());
		}
		
		System.out.println("Complete.");
	}

}
