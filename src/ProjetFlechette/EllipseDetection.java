package ProjetFlechette;

import java.util.ArrayList;

public class EllipseDetection {
	
	ArrayList<Ellipse> ellipses = new ArrayList<Ellipse>();
	
	public EllipseDetection(){
		
	}
	
	public void detectEllipse(ImagePerso img, int width, int height, int minor, int major, int seuil){
		//Publication : http://bit.ly/2qeSTGb
		//Explication d'un autre internaute : http://bit.ly/2oSBdOF
		
		int MIN_HALF_MAJOR = major;
		int MIN_HALF_MINOR = minor;
		
		
		ArrayList<Vector2> PixelContours = new ArrayList<Vector2>();
		img.luminance();
		img.seuillage();
		Sobel sob = new Sobel();
		img = sob.apply(img);
		img.seuillage();
		
		int[] accumulateur = new int[width*height];

		
		//on ajoute les pixels blancs (de contour donc apr�s sobel) dans un tableau
		for(int i=0; i<img.height; i++){
			for(int j=0; j<img.width; j++){
				if(img.img[i][j][0] == 255){
					PixelContours.add(new Vector2(j,i));
				}
			}
		}
		
		//clear accumulator Array - voir HOUGH
		
		for(int i=0; i<PixelContours.size(); i++){ //les pixels (x1,y1)
			for(int j=0; j<PixelContours.size(); j++){ //les pixels (x2,y2)
				
				double var1 = Math.pow(PixelContours.get(j).x - PixelContours.get(i).x, 2);
				double var2 = Math.pow(PixelContours.get(j).y - PixelContours.get(i).y, 2);
				double distance = distanceAB(PixelContours.get(j).x,PixelContours.get(j).y,PixelContours.get(i).x,PixelContours.get(i).y);
				
				//if distance is greater than required least distance for a pair of pixels to be considered
				if(distance > MIN_HALF_MAJOR){
					//faire etape 4 : avec equation calcul des centre, orientation, taille de l'axe majeur
					int x0 = (PixelContours.get(j).x + PixelContours.get(i).x)/2; //centre x
					int y0 = (PixelContours.get(j).y + PixelContours.get(i).y)/2; //centre y
					double a = (Math.pow((var1 + var2),1/2))/2; //orientation
					double alpha =0;
					if((PixelContours.get(j).x - PixelContours.get(i).x) != 0){
						alpha = Math.atan((PixelContours.get(j).y - PixelContours.get(i).y)/(PixelContours.get(j).x - PixelContours.get(i).x)); //taille de l'axe majeur
					}
									
					// (6) Pour chaque pixels pixel si la distance entre (x,y) et (x0,y0) (le centre) > required least distance for a pair of pixels faire (7) a (9)
					
					
					int b = 0;
					double bSquare =0;
					for(int k=0 ; k<PixelContours.size(); k++){

						double distance2 = distanceAB(x0 , y0 ,  PixelContours.get(i).x,  PixelContours.get(i).y);
						
						if(distance2> MIN_HALF_MINOR){
							//(7) utiliser les equation 5 et 6 pour calculer l'axe mineur de l'ellipse
							double aSquare = Math.pow(a,2);
							double distance2Square = Math.pow(distance2,2); //correspond a d2 
							double f = Math.sqrt((Math.pow(PixelContours.get(k).x - PixelContours.get(j).x, 2))+(Math.pow(PixelContours.get(k).y - PixelContours.get(j).y, 2)));
							double fSquare = Math.pow(f, 2);
							double cost = (aSquare + distance2Square - fSquare)/(2*a*distance2);
							double costSquare = Math.pow(cost, 2);
							double sintSquare = 1 - costSquare;
							//cos�(alpha) + sin�(alpha)  = 1  || 
							bSquare = (aSquare*distance2Square*sintSquare)/(2*a*distance2); //distance entre (x,y) et (x0,y0)
							//(8) incr�menter l'accumulateur de la taille de l'axe mineur de 1 
							b = (int)Math.sqrt(bSquare);
							//(9) boucler pour que tout les pixels soient calcul� avec cette pair de pixels
							
							
							accumulateur[b] +=1; //on incr�mente l'accumulateur pour cette valeur d'axe mineur
							
						}
						
						
					}
					
					int max = -666;
					for(int k=0; k<accumulateur.length; k++){
						if(accumulateur[k] > max)max = accumulateur[k];
					}
					
					if(max > MIN_HALF_MINOR){ //l'ellipse est valable on l'ajoute dans le tableau !
						this.ellipses.add(new Ellipse(new Vector2(x0,y0),alpha,max,a));
					}
					
					accumulateur = new int[width*height]; //on vide l'accumulateur pour tester l'ellipse suivante
						
						//ellipse found
						
						
						
		//(10) Trouver le maximum dans l'accumulateur. Cela correspond a la valeur potentielle de l'axe mineur de l'ellipse 
		//(10) bis : si le vote est plus grand que le nombre requis pour l'ellipse en cours, alors une ellispe est trouv�e
		
		//(11) retourn� les param�tres de l'ellipse
		//(12) enlever les pixels de l'ellipse detect� de PixelContours
		//(13) effacer l'array accumulator
		//(15) Ajouter les ellipses sur l'image finale 
					
					
				}
			}
		}//(14) Boucler jusqu'a ce que tout les pixels soient calcul�s 
		
		
		
	}
	
	public double distanceAB (int x, int y, int x1, int y1){
		double var3 = Math.pow(x1 - x, 2);
		double var4 = Math.pow(y1 - y, 2);
		return Math.sqrt(var3+var4);
		
	}
}
