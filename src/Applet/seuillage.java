package Applet;

public class seuillage {
	
	static int[] input;
	static int[] output;
	int width;
	int height;
	
	public seuillage(int[] inputIn, int widthIn, int heightIn){
		width=widthIn;
		height=heightIn;
		input = new int[width*height];
		output = new int[width*height];
		input=inputIn;
	}
	
	public int[] process(){
		int seuil =20;
		int progress =0;
		for(int x=0;x<width;x++) {
			progress++;
			for(int y=0;y<height;y++) {
				int value = (input[y*width+x]) & 0xFF; 
				if (value > seuil) {
					output[y*width+x] =0xFFFFFFFF ;
				}
				else
				{
					output[y*width+x] = 0xFF000000;
				}
			}
		}
		return output;
	}
}
